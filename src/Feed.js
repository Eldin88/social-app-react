import React from 'react'
import "./Feed.css";
import MessageSender from "./MessageSender";

function feed() {
    return (
        <div className="feed">
            <MessageSender  />
        </div>
    )
}

export default feed
