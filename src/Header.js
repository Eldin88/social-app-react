import React from 'react'
import "./Header.css";
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import EmojiFlagsIcon from '@material-ui/icons/EmojiFlags';
import StoreIcon from '@material-ui/icons/Store';
import SubscriptionsIcon from '@material-ui/icons/Subscriptions';
import GroupIcon from '@material-ui/icons/Group';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from"@material-ui/icons/Add"; 
import ForumIcon from '@material-ui/icons/Forum';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


function Header() {
    return <div className="header">
        <div className="header_left">

            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-Wo7zePo6PonrgCUERD-9Q0ZG64CrBbZAIw&usqp=CAU" alt="logo"></img>
      
            <div className="header_input">
            <SearchIcon />
            <input placeholder="search" type="text"></input>
            
             </div>
    
        </div>
        

        <div className="header_center">
            <div className="header_option">
                <HomeIcon fontSize="large" />
            </div>

            <div className="header_option">
                <EmojiFlagsIcon fontSize="large" />
            </div>

            <div className="header_option">
                <SubscriptionsIcon fontSize="large" />
            </div>

            <div className="header_option">
                <StoreIcon frontfontSize="large" />
            </div>

            <div className="header_option">
                <GroupIcon frontfontSize="large" />
            </div>

        </div>

        <div className="header_right"></div>
        <div className="header_info">
        <AccountBoxIcon />
        <h4>Eldin</h4>
        </div>

        <IconButton />
        <AddIcon />
        <IconButton />
        <IconButton />
        <ForumIcon />
        <IconButton />
        <IconButton />
        <NotificationsActiveIcon />
        <IconButton /> <IconButton />
        <ExpandMoreIcon />
        <IconButton />

    </div>

}

export default Header; 